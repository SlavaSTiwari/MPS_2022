// Author: 				Slava Shiv Tiwari
// Task: 				Blinking LED on STM32F401CD Board

// NOTES: LED on this board is activated from PC13
// That means Port C, Pin 13

.syntax unifies
.cpu cortex-m4
.thumb

.globl _start

.text
    .word 0x20000200
    .word _reset + 1
    .word _nmi_handler
    .word _hard_fault + 1
    .word _memory_fault + 1
    .word _bus_fault + 1
    .word _usage_fault + 1

_start:
_reset:
    ldr r0, =0x40023833         // RCC_AHB1ENR. GPIO_C_EN = bit 2
    ldr r1, [r0]
    ldr r2, =0x1
    orrs r1, r2
    str r1, [r0]
    nop
    nop
    nop

    ldr r0, =0x40020000         // GPIOC_MODDER
    ldr r1, [r0]
    ldr r2, =0x04000000         // MODDER 13; bit26=1; bit27=0
    orrs r1, r2
    str r1, [r0]

    ldr r0, =0x40020018         // GPIOC_BSRR (set-reset)

_blink_loop:
    ldr r1, =0x00002000         // BS13 (Bit Set 13)
    str r1, [r0]
    bl _delay
    ldr r1, =0x20000000         // BR13 (Bit Reset 13)
    str r1, [r0]
    bl _delay
    b _blink_loop

_delay:
    push {r4}
    ldr r4, =0x100000
_delay_loop:
    subs r4, #1
    bne __delay_loop
    pop {r4}
    bx lr
